// single line comments = //
// prints a simple text in the console

console.log('Hello World!');
console.log('Hello again!');

// multi line comment /**/
// ctrl+shift+//
/*The quick brown fox
jumps over the lazy dog*/

/*VARIABLES*/
// keyword variable_name = value;

// name = 'john';
let name = 'cath'; //initialization or declaration
name = 'juan';
console.log(name);

let phrase = 'he quick brown fox jumps over the lazy dog';
console.log(phrase);

const age = 18;
// age = 12;
console.log(age);

const interest = 4;
// interest = 10;

var firstname = 'jane';
console.log(firstname);

let lastname;
console.log(lastname);
//undefined

// lowercase
let lowercase;

// snake case
let snake_case;

// camel case
let camelCase;

//bbq case
// let phone-number;

/*DATA TYPES*/
let string = 'hello world';
let number = 42;
let boolean = true; //true/false

// array = collection of data
let array = [5, 9, 64];
let anotherArray = ['hello', 7, false];
console.log(anotherArray);

let fruitsArray = ['Orange', 'Apple', 'Kiwi'];

//object
let person = {
	firstname: 'john',
	lastname: 'doe',
	age: 23,
	employed: true,
	nickname: ['jay', 'joe']
}; 

let cellphone = { brand: 'samsung', price: 25000 };

// undefined = absence of a value
let address;
console.log(address);

// null = value is blank
let city = null;
console.log(city);

/*==========================ASSIGNMENT OPERATOR==================================*/
/*----------------------------math operators-------------------------------------*/
// addition operator
let num1 = 2;
// num1 += num1; //4
num1 += 3; //5
// num1 = num1 + 3; //num1 = 2 + 3;
num1 += 5; //10
console.log(num1);

// subtraction operator
num1 -= 8; //2
console.log(num1);

// multiplication operator
num1 *= 2;//4
console.log(num1);

let num2 = 3;
// division operator
// num1 /= 4;
num1 /= num2 *= 10; //PEMDAS
console.log(num1);

/*---------------------------arithmetic operators------------------------------*/
let x = 1;
let y = 2;
let sum = x + y; //3
console.log(sum); 

let difference = x - y; //-1
console.log(difference);

let product = x * y; //2
console.log(product);

let quotient = x / y;
console.log(quotient);

// modulo/remainder
let remainder = x % y;
console.log(remainder);

// increment
let increment = ++y; //3
console.log(increment);

//decrement
let decrement = --x; //0
console.log(decrement);

let total = 1 + 2 * 3;
console.log(total);

//type coercion
let num3 = '1';
let num4 = '2';

console.log(num3 + num4); //concatination
num3 = parseInt(num3);
num4 = parseInt(num4);
console.log(num3 + num4);

/*comparison operators*/
//equality operator
let myName = 'juan';
let myNum = '1';
console.log(myName == 'juan');//true
console.log(myNum == 1);//true

//inequality operator !=
// ! not operator
console.log(myNum != 1);//false
console.log(myNum != 2);//true

//strict=equality operator ===
console.log(myNum === 1);//false
console.log('1' === 1);//false
console.log(1 === 1);//true

//strict-inequality operator !==
console.log('1' !== 1);//true
console.log(myNum !== '1');//false

//relational operators
// greater than >
console.log(2 > 1);//true

//less than <
console.log(2 < 1);//false

//greater than or equal to >=
console.log(2 >= 2);//true

//less than or equal to <=
console.log(1 <= 0);//false

/*logical operators*/
let isLegalAge = true;
let isMarried = true;
let isRich = false;

// and &&
console.log(isLegalAge && isMarried); //true
console.log(isMarried && isRich); //false
console.log(isLegalAge && isMarried && isRich); //false

// or ||
console.log(isMarried || isRich); //true
console.log(isLegalAge || isMarried || isRich); //true

// not ! checks for the opposite value
console.log(!isLegalAge); //false
console.log(!isLegalAge && !isMarried); //false
console.log(!isLegalAge || !isMarried); //false
console.log(!isLegalAge && !isRich); //false
console.log(isLegalAge && !isRich); //true
console.log(isLegalAge && isMarried || isRich); //true

/*FUNCTIONS*/

console.log(1 + 2);
console.log(2 + 3);

addNumbers(20, 10);
function addNumbers(num1, num2){
	console.log(num1 + num2);
};
addNumbers(4,5);

function sayHello(){
	console.log('Hello');
};

function sayMyName(name){
	console.log('My name is ' + name);
	console.log('hello');
};

sayMyName('Job');


/*
function functionName(parameters){
	//block of code goes here
};
*/

function homeAddress(){
	let hNum = 'L10 Blk1 Stanley Ville'
	let brgy = 'San Agustin'
	let city = 'Malolos City'
	let province = 'Bulacan'
	let country = 'Philippines'
	console.log(`${hNum}, ${brgy}, ${city}, ${province}, ${country}`)
}
homeAddress();
